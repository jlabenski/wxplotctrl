
README for wxPlotCtrl
Copyright : 2006, John Labenski
License : wxWindows license.

This library/program is free software; you can redistribute it and/or modify
it under the terms of the wxWindows License; either version 3 of the License,
or (at your option) any later version.

This library is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
or FITNESS FOR A PARTICULAR PURPOSE. See the wxWindows License for more details.

If you use this program/library and find any bugs or have added any features 
that you feel may be generally useful, please feel free to 
contact the author by e-mail at jrl1[at]sourceforge[dot]net.

Summary :
wxPlotCtrl is a plotting widget and library for the wxWidgets cross-platform 
C++ GUI library. It is designed as an X-Y interactive plotting widget that 
allows for easy user manipulation of the data using the mouse or the keyboard. 
It does not implement pie, bar, or surface plots. In addition to the widget 
itself are a number of data container classes for loading, saving, and 
manipulating plot data, selecting points, function fitting, and arbitrary function plotting.

It has been tested in MS Windows OS >= 95 and *nix using GTK 1.2 or 2.0.
The recommended version of wxWidgets is 2.8.x.
References :
http://wxcode.sourceforge.net
http://www.wxwidgets.org
http://bakefile.sourceforge.net

Provides : (at a glance)

-   wxPlotCtrl - The main plotting widget with axes and scrollbars.
    -   Create as you would a wxWindow and
-   wxPlotCtrlArea - A wxWindow child of the wxPlotCtrl where the data is plotted.
    -   This window forwards events to the virtual functions wxPlotCtrl::ProcessAreaEVT_XXX() which can be easily overridden.
-   wxPlotCtrlAxis - Either an X or Y axis wxWindow child of the wxPlotCtrl.
    -   This window forwards events to the virtual functions wxPlotCtrl::ProcessAxisEVT_XXX() which can be easily overridden.
-   wxPlotCtrlEvent - Events from the wxPlotCtrl.
    -   See the bottom of plotctrl.h for descriptions of the event types.
-   wxPlotCtrlSelEvent - Selection events from the the wxPlotCtrl.
    -   See the bottom of plotctrl.h for descriptions of the event types.
-   wxPlotCurve - wxObject derived base class for a "curve" that stores a bounding rect, pens, ability to store string options, and is a wxClientData container.
    -   New curves can be derived from this class, but you must override:
        -   wxPlotCurve* Clone() const - create a "new" refed instance of the curve.
        -   virtual bool Ok() const - Ref data exists and the curve is Ok.
        -    virtual double GetY( double x ) const - This is the function y = f(x) for the curve.
        -    The wxObject ref data must be derived from wxPlotCurveRefData and you should update m_boundingRect as appropriate.
    -   The colors for the curve are stored in an array and are initialized at creation from a static array. You can change the default colors and/or adjust them after creation.
-   wxPlotData - A data curve, load, save, copy, resize, search, offset, add, abs, deviation, variance, FFT, power spectrum, FFT filters (high, low, notch, butterworth, custom)...
-   wxPlotFunction - Arbitrary function curve, plots the function given as a string.
-   wxPlotDrawerBase - Base class for drawing, plot area, axis, key, curves...
    -   This class stores some useful information commonly used in the derived drawers.
-   wxPlotDrawerArea - Not used.
-   wxPlotDrawerAxisBase - Base class for the X or Y axis drawer.
    -   wxPlotDrawerXAxis - Draws an X axis.
    -   wxPlotDrawerYAxis - Draws an Y axis.
-   wxPlotMarker - Arbitrary markers that can be added to the plot.
    -   This class stores values for a marker and the drawing is done by the wxPlotDrawerMarker.
-   wxPlotPrintout - WYSIWYG printing for the plot control.
-   LM_LeastSquare - Levenberg-Marquart nonlinear least squares 2-D curve fitting using a wxPlotFunction to find to a wxPlotData.

Compilation :
Compiles for wxWidgets version 2.6 and up, recommended version >= 2.8
Tested in Linux using gcc and wxGTK and wxMSW using MSVS 6&7.

Linux using GCC and gmake :
Use the Makefile in the src or samples/plotctrl directory which depends on the 
wxWidget's 'wx-config' script being in the path. The Makefile in the 
/src directory will create a library in the wxWidgets/lib directory using 
the wxWidgets naming conventions. To create the library elsewhere, 
change the variable $(WXLIBDIR) in the Makefile.

The Makefile in the samples/plotctrl dir will automatically try to build the 
library first and so you can simply run make in the samples/plotctrl dir to build both.

MSW using MSVS :
Use the dsw file in the build dir to build both the sample and the library. 
You must have the WXWIN environment variable set to the location of your 
copy of the wxWidgets source to use the project files. In MS Win 9x you must 
edit your config.sys file and reboot. In 2k/XP you can right click on 
“My Computer->Properties->Advanced->Environment Variables” and add a user variable, 
you must restart MSVS for the change to take effect.

Bakefile :
I am unable to test all of these configurations, the Microsoft 2003 free 
compiler has been tested however. Use these make and project files in the 
same way that you would use them in the wxWidget's samples directory.

Normally, you do not have to regenerate these... In the build directory is a 
bakefile called "wxplotctrl.bkl”. To regenerate the make and project files run 
“bakefile_gen wxplotctrl.bkl”, to regenerate a single type, like the 
Visual Studio project files for example, use “bakefile_gen -f msvs stedit.bkl”.
The base files for bakefile are, wxplotctrl.bkl (the main bakefile), 
Bakefile.bkgen (edit to point to wxplotctrl.bkl), and for gcc in linux 
aclocal.m4, acregen.sh, config.guess, config.sub, configure.as.

Documentation :
The "full documentation" is included in the headers with a description of the 
public functions. There is also a doxygen.cfg file in the "docs" directory 
that you can use to create cross referenced html docs, please review and 
adjust the file to your liking.

wxplotctrl.cpp : samples/plotctrl/wxplotctrl.cpp
Sample program that uses the wxPlotCtrl to load xy data and plot arbitrary 
functions. It shows most of the bool functions of the wxPlotCtrl class and 
prints all the events to a log window.

#defines and enums : include/wx/plotctrl/plotdefs.h
TODO - write more docs, for now see the headers.


